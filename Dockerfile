FROM python:3

WORKDIR /app

COPY . .

RUN bash ./scripts/install.sh

CMD [ "bash", "./scripts/notebook.sh" ]
