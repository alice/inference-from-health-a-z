# Inference From Health A-Z

In this project, we use data from the NHS's Health A-Z catalogue to create a conversational symptom checker.


## Download

The project is packaged with Docker. Use the following commands to download and run it locally:

```
git clone https://codeberg.org/alice/inference-from-health-a-z.git
```

```
cd ./inference-from-health-a-z
```

```
chmod +x ./scripts/*
```

```
./scripts/start.sh
```

Jupyter should give you a link to the notebook session. Visit the URL starting with `http://127.0.0.1:8888` in your browser.
