# pipery, a minimal functional pipelining framework. 


from functools import reduce


# Apply a function to each value of an iterable.

def Lambda(function):
    
    def ret(iterable):
        
        for value in iterable:
            try:
                yield function(value) or value
            except: pass
    
    return ret


# Remove falsy values from an iterable according to a predicate.

def Filter(predicate):
    
    def ret(iterable):
        for value in iterable:
            if predicate(value): yield value
    
    return ret


# Perform a reduction on an iterable using a given function.

def Reduce(function):
    
    def ret(iterable):
        result = next(iterable)
        
        for value in iterable:
            result = function(result, value)
        
        yield result
    
    return ret


# The spread operator but for iterators.

def Spread(iterator):
    
    def ret(iterable):
        
        for value in iterable:
            for nested in iterator(value):
                yield nested
    
    return ret


# Group the values of an iterable into a single collection.

def Collect(iterable):
    yield list(iterable)


# Simplify the creation of iterator pipelines.

def Pipe(*iterators):
    
    def ret(iterable):
        return reduce(lambda a, b: b(a), iterators, iterable)
    
    return ret