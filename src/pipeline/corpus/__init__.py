from pipery import Pipe
from . scraping import scraping
from . preprocessing import preprocessing


corpus = Pipe(scraping, preprocessing)