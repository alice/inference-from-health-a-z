from pyquery import PyQuery
from pipery import Lambda
from pipery import Pipe
from . variables import Annotations
from . annotate import annotate
from . textualise import textualise
from . normalise import normalise


preprocessing = Pipe(
    annotate(Annotations.add),
    Lambda(PyQuery),
    textualise,
    normalise,
    annotate(Annotations.sub),
)