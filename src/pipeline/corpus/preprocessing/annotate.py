from replace import replace
from pipery import Lambda


def annotate(annotations):
    
    return Lambda(lambda text: replace(text, annotations))