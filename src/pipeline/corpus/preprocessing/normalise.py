from normality import normalize
from normality import slugify
from pipery import Lambda


# Normalise an article's heading and content. Note that annotations 
# will be preserved.

@Lambda
def normalise(parsed):
    
    return f'{slugify(parsed.heading)}\n{normalize(parsed.content)}'