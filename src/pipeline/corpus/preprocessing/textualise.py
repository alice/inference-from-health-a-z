from pipery import Lambda
from . variables import heading
from . variables import content


# Extract relevant text from a parsed article; namely the article's
# heading, and content.

@Lambda
def textualise(parsed):
    
    parsed.heading = parsed(heading).text()
    parsed.content = parsed(content).text()