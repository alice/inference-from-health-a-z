# Annotations used to preserve some textual semantics after 
# normalisation.

class Annotations: pass


# Add the annotations.

Annotations.add = {'.': 'xxyxyxyy'}


# Remove the annotations.

Annotations.sub = {'xxyxyxyy': '.'}


# CSS selectors used to extract relevant content from each article.

heading = 'h1'
content = 'article section p, article section ul'