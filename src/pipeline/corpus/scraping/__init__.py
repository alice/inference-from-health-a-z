from pipery import Lambda
from pipery import Pipe
from . scrape import scrape
from . download import download


scraping = Pipe(
    scrape,
    Lambda(lambda url: download(url[0]))
)