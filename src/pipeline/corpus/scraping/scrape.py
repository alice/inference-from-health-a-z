from re import findall as search
from pipery import Spread
from . download import download
from . variables import pattern


scrape = Spread(lambda url: search(pattern, download(url)))